require 'ostruct'

module Commands
  class Config < OpenStruct
    def set(key, value)
      send("#{key}=", value)
    end
  end
end

