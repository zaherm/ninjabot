require 'commands/store'
module Commands
  class Router
    def initialize(store = Commands::Store.new)
      @store = store
    end

    def route(path, key = nil)
      npath = @store.get(key)
      return parse(npath || path)
    end

    def parse(path)
      ns, p = nil, nil
      if path[0] == '/'
        ns = path[/\/(.[^\s\.]+)/, 1]
        path.slice!("/#{ns}")
        path.strip! if path
      end
      return [ns.to_sym, path] if ns
      [nil, nil]
    end
  end
end
