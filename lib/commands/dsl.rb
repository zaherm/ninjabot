require 'commands/core'

def config(&block)
  Commands::Core.config(&block)
end

def namespace(namespace, &block)
  Commands::Core.namespace(namespace, &block)
end

def invoke(path, args = {})
  Commands::Core.invoke(path, args)
end

def help
  result = []
  Commands::Core.commands.each { |key, cmd|
     result << "/#{key} - #{cmd.desc}"
  }
  result.join("\n")
end
