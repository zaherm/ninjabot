module Commands
  class Store
    def initialize
      @data = {}
    end

    def get(key, default = nil)
      @data[key] || default
    end

    def set(key, value)
      @data[key] = value
    end

    def unset(key)
      @data[key] = nil
    end
  end
end
