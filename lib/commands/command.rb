require 'commands/scope'
module Commands
  class Command
    def initialize(namespace)
      @namespace = namespace
      @blocks = {}
    end

    def on(path, &block)
      @blocks[path] = block
    end

    def desc(description = nil)
      @desc ||= description
    end

    def invoke(text, args)
      key, block = *find(text)

      if block
        scope = Commands::Scope.new(args)
        begin
          scope.text = text.gsub(key, '').strip
          scope.instance_eval(&block)
        rescue Exception => e
          scope.text = e.message
          scope.error = true
        end
        return scope
      end
    end

    def find(text)
      block = @blocks[:default]
      key = ""
      return [key, block] if text.nil? || text.empty?
      subcommand = text.split(" ").first.to_sym
      @blocks.each { |k,b|
        if k.is_a?(Regexp)
          if text =~ k
            block = b
            key = '' #we don't want to strip the regex, so it's accessible as 'text'
          end
        end
        if k.is_a?(Symbol)
          if subcommand == k
            block = b
            key = k.to_s
          end
        end
      }
      [key, block] #.first
    end
  end
end
