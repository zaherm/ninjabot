require 'ostruct'
module Commands
  class Scope < OpenStruct
    def method_missing(method_sym, *arguments, &block)
      if Commands::Core.config.respond_to?(method_sym)
        Commands::Core.config.send(method_sym, *arguments, &block)
      else
        super(method_sym, *arguments, &block)
      end
    end

    def initialize(hash = nil)
      super(hash)
    end

    def set(key, value)
      send("#{key}=", value)
    end

    def get(key, default = nil)
      send("#{key}") || default
    end

  end
end

