require 'singleton'
require 'yaml'
require 'rake_text'

module Commands
  class Transformer
    include Singleton

    def self.transform(text)
      begin
        r = analyze(text)
        match(r)
      rescue Exception => e
        nil
      end
    end


    def self.analyze(text)
      a = rake.analyse(text, RakeText.SMART)
      a.map { |k,v| k }.join(' ')
    end

    def self.match(analysis)
      ns, command, text, = nil, nil, nil
      config.each { |n, c|
        a = analysis.gsub(/\s?#{c['stoplist'].join('|')}\s?/i, '')
        ns = a.slice!(/(#{n})/)
        command = a.slice!(/(#{c['commands'].join('|')})/)
        text = a.strip
        break if ns # && command && text
      }
      rest = []
      rest << command if command
      rest << text if text
      [ns, rest.join(' ')]
    end

    def self.rake
      @rake ||= RakeText.new
    end

    def self.config
      @config ||= YAML::load_file("./config/transformer.yml")
      @config
    end

    def self.config=(config)
      @config = YAML::load_file("./config/transformer.yml")
    end
  end
end
