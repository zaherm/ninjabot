require 'singleton'
require 'commands/config'
require 'commands/command'
require 'commands/scope'
require 'commands/router'
require 'commands/transformer'
require 'logger'

module Commands
  class Core
    include Singleton

    def self.config(&block)
      @config ||= Commands::Config.new({})
      if block_given?
        @config.instance_eval(&block)
      end
      @config
    end

    def self.commands
      @commands
    end

    def self.namespace(namespace, &block)
      @commands ||= {}
      raise "path = '#{path}' already in use" if @commands[namespace]
      command = Command.new(namespace)
      command.instance_eval(&block)
      @commands[namespace] = command
    end

    def self.invoke(path, args = {})
      ns, rest = *route(path, args[:routing])
      unless ns
        ns, rest = *transform(path)
      end
      return unless ns
      command = @commands[ns]
      if command
        r = command.invoke(rest, args)
        if r.get(:next)
          store.set(args[:routing], "/#{ns} #{r.get(:next)}")
        else
          store.unset(args[:routing])
        end
        return r
      end
    end

    def self.route(path, key = nil)
      npath = store.get(key)
      return parse(npath || path)
    end

    def self.parse(path)
      ns = nil
      if path[0] == '/'
        ns = path[/\/(.[^\s\.@]+)/, 1]
        path.slice!("/#{ns}")
        path.gsub!(/^@.[^\s]+/, '') if path
        path.strip! if path
      end
      return [ns.to_sym, path] if ns
      [nil, nil]
    end

    def self.transform(text)
      r = Transformer.transform(text)
      return [nil, nil] unless r
      ns, rest = *r
      return ns.nil? ? [nil, nil] : [ns.to_sym, rest]
    end

    def self.store
      @config.store ||= Commands::Store.new
    end
  end
end
