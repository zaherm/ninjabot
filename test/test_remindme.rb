#!/usr/bin/env ruby
$:.unshift File.join(File.dirname(__FILE__), '..', 'src')
require 'tasks'
require 'ostruct'
logger = Logger.new('ninjabot.log', 7)
redis = Redis.new

tasks = Tasks.tasks(logger, redis)
tasks.each { |t, o|
  puts "t = #{t}"
}
remindme = tasks['remindme']
user = OpenStruct.new
user.id = "-1"
result = remindme.send(:start, user, '')
if result[:next] == :when
  result = remindme.send(:when, user, 'tomorrow at 2pm')
end
if result[:next] == :what
   puts "got what"
  result = remindme.send(:what, user, 'test')
end

puts redis.get("scheduler:#{user.id}")
