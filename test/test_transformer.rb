$:.unshift File.join(File.dirname(__FILE__), '..', 'lib')
require 'commands/transformer'
require 'yaml'

Commands::Transformer.config = YAML::load_file("./config/transformer.yml")
#puts Commands::Transformer.config.inspect
texts = [
  "tel aviv's weather",
  "what's the weather in tel aviv",
  "what is going on in reddit",
  "get the latest item in reddit programming",
  "chuck",
  "insult zaher",
  "get random chuck fact about zaher",
  "chuck zaher",
  "ping",
  "yo",
  "search for ninjas",
  "giphy cat",
  "random xkcd",
  "latest xkcd",
  "wiki zigi marely"
]
texts.each { |t|
  r = Commands::Transformer.transform(t)
  puts "/#{r}"
}
