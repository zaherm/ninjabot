require './src/scheduler.rb'
require 'logger'
require 'redis'
require 'securerandom'
require 'time'
class Reciever
  def on_job(job)
    if job['type'] == 'message'
      message = OpenStruct.new
      message.user = OpenStruct.new
      message.user.id = job['user_id']
      message.text = job['text']
      message.chat = OpenStruct.new
      message.chat.id = job['chat_id']
      message.task_key = job['task_key']
      on_message(message)
    end
  end

  def on_message(message)
    puts message.inspect
  end
end

job = {
  'type' => 'message',
  'text' => 'Text goes here',
  'user_id' => 1,
  'chat_id' => 2,
  'timestamp' => Time.now.utc.to_s
}
r = Reciever.new 
sched = Scheduler.new(Logger.new(STDOUT), r)
sched.enqueue('yo', 1, 1, job)
sched.start!

