
require 'logger'
require 'singleton'
class Scope
#  include Singleton
  attr_accessor :message, :user, :text
  def logger
    @logger ||= Logger.new(STDOUT)
  end
end

class Tasks
  include Singleton
  @tasks = {}
  def self.define(pattern, &block)
    @tasks[pattern] = block
  end

  def self.invoke(pattern, m, u, t)
    s = Scope.new
    s.message = m
    s.user = u
    blk = @tasks[pattern]
    return s.instance_eval(&blk)
    #@@tasks[pattern].call(a)
  end

  def method_missing(meth_name)
    puts "missing = #{meth_name}"
  end
end


Tasks.define 'abc' do
  puts message
  logger.info user
  'result'
end

puts Tasks.invoke('abc', "msg", "user", "some text")
