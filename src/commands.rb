$:.unshift File.join(File.dirname(__FILE__), '..', 'lib')
require 'commands/dsl'
require 'logger'
require 'redis'
require 'json'
require 'faraday'
require 'faraday_middleware'
require 'sanitize'
require 'google-search'
require 'nokogiri'
require 'stock_quote'
require 'utils'
require 'facter'
require 'giphy'
require 'open_weather'
require 'time'
require 'foaas-client'

config do
  set :logger, Logger.new(STDOUT)
  set :redis, Redis.new
  set :fuck, Foaas::Client.new
end

namespace :help do
  desc "Show help"
  on :default do
    set :text, help
  end
end

namespace :chuck do
  desc "Generate a random Chuck Norris fact"
  on :default do
    params = { :firstName => user.first_name || 'Chuck', :lastName => user.last_name || 'Norris' }
    json = Utils.get_json("http://api.icndb.com/jokes/random" , params)
    set(:text, json['value']['joke'])
  end

  on /.+/ do
    parts = text.split(' ')
    params = { :firstName => parts[0] || 'Chuck', :lastName => parts[1] || 'Norris' }
    json = Utils.get_json("http://api.icndb.com/jokes/random" , params)
    set(:text, json['value']['joke'])
  end

end

namespace :yo do
  desc "Replies with 'Yo!'"
  on :default do
    set(:text, "Yo!")
  end
end

namespace :ping do
  desc "Replies with 'Pong!'"
  on :default do
    set(:text, "Pong!")
  end
end

namespace :google do
  desc "Perform a Google web search"
  on :default do
    web = ::Google::Search::Web.new do |s|
      s.query = text
    end
    item = web.find.first
    title = Sanitize.fragment(item.title)
    content = Sanitize.fragment(item.content)
    set(:text, "#{title}\n#{content}\n#{item.uri}")
  end
end

namespace :fuck do
  desc "Generate a random insult for a given name"
  on :default do
    res = fuck.everyone('Ninjabot')
    set(:text, res['message'])
  end

  on /.+/ do
    target = text[/\@(.+)/,1]
    rest = text.gsub("@#{target}", "").split(" ").map(&:strip)
    action = rest.first
    rest -= [action]
    rest += ['Ninjabot']
    text = ""
    logger.info "target='#{target}', action='#{action}"
    text = "@#{target}, " + fuck.send(action.to_sym, *rest)['message']
    set(:text, text)
  end
end

namespace :wiki do
  desc "Perofme a Wikipedia search"
  on :default do
    json = Utils.get_json('https://en.wikipedia.org/w/api.php', { :list => :search, :srsearch => text, :action => :query, :format => :json })
    item = json['query']['search'].first rescue nil
    if item
      url = "https://en.wikipedia.org/wiki/#{item['title'].gsub(/\s/,'_')}"
      set(:text, url)
    else
      set :text, "not found '#{text}'"
    end
  end
end

namespace :quote do
  desc "Get a random quote"
  on :default do
    response = Faraday.post("http://www.quoteland.com/random.asp", { :PAGESIZE => '1' })
    doc = Nokogiri::HTML(response.body)

    category = doc.css("font[size='3'] b")[1].text
    quote = doc.css("td font[color='#333333']")[1].text
    author = doc.css("td font[size='2'] i a").text
    result =<<EOF
    "#{quote}"
         - #{author} (#{category})
EOF
    set(:text, result)
  end
end

namespace :stock do
  desc "Get stock information"
  on /.+/ do
    stock = StockQuote::Stock.quote(text) rescue nil
    return "No stock data for #{symbol}" unless stock
    result =<<EOF
  Symbol: #{stock.symbol}
  Name: #{stock.name}
  Exchange: #{stock.stock_exchange}
  Current: #{stock.ask}
  Open: #{stock.open}
  Percent: #{stock.changein_percent}
EOF
  set(:text, result)
  end
end

namespace :reddit do
  desc "Get latesr/random item from a given subreddit"
  on :default do
    item = Utils.get_json("http://www.reddit.com/r/#{text}/.json")['data']['children'].sample
    result =<<EOF
#{item['data']['title']}
#{item['data']['url']}
https://www.reddit.com#{item['data']['permalink']}
EOF
    set(:text,result)
  end

  on :random do
    item = Utils.get_json("http://www.reddit.com/r/#{text}/.json")['data']['children'].sample
    result =<<EOF
#{item['data']['title']}
#{item['data']['url']}
https://www.reddit.com#{item['data']['permalink']}
EOF
    set(:text,result)
  end

  on :latest do
    item = Utils.get_json("http://www.reddit.com/r/#{text}/.json")['data']['children'].first
    result =<<EOF
#{item['data']['title']}
#{item['data']['url']}
https://www.reddit.com#{item['data']['permalink']}
EOF
    set(:text,result)
  end
end

namespace :yomomma do
  desc "Get a random YoMomma joke"
  on :default do
    joke = Utils.get_json("http://api.yomomma.info/")['joke']
    set(:text, joke) unless joke.nil?
  end
end

namespace :track do
  desc "PostIL package tracking"
  on :default do
    set(:text, "Please enter an item code")
    set(:next, :add)
  end

  on /add/i do
    redis.hset("#{user.id}.track", text, 'new')
    set(:text, "Item code added")
  end


  on :status do
    logger.info "track.status"
    codes = redis.hgetall("#{user.id}.track").keys
    codes.each do |code|
      params = { '_' => rand(), 'itemcode' => code, 'lang' => 'EN', 'sKod2' => '', 'openagent' => '' }
      res = Faraday.get 'http://www.israelpost.co.il/itemtrace.nsf/trackandtraceJSON', params
      response_data = JSON.parse(res.body) rescue {}
      rdata = response_data['itemcodeinfo']
      result = ""
      if rdata =~ /No information/
        result = "No information for this item"
      else
        doc = Nokogiri::XML(rdata)
        rows = doc.xpath('//table/tr')
        result = []
        rows.reverse.each { |tr|
          tds = tr.xpath('td')
          next if tds[0].text.strip == 'Date'
          result << "- #{tds[0].text}, #{tds[1].text} (#{tds[2].text})"
        }
        result = result.join("\n")
        redis.hset("#{user.id}.track", code, result)
      end
    end
    status = []
    all = redis.hgetall("#{user.id}.track")
    all.each { |code, result|
      status << code
      status << "-----"
      status << result
    }
    set(:text, status.join("\n"))
  end
end

namespace :xkcd do
  desc "Get latest/random xkcd item"
  on :default do
    item = Utils.get_json("http://xkcd.com/info.0.json")
    result =<<EOF
#{item['safe_title']}
#{item['alt']}
EOF
    set(:photo, Utils.download_file(item['img']))
    set(:caption, result)
  end

  on :random do
    last_num = Utils.get_json("http://xkcd.com/info.0.json")["num"]
    item = Utils.get_json("http://xkcd.com/#{rand(last_num)}/info.0.json")
    result =<<EOF
#{item['safe_title']}
#{item['alt']}
EOF
    set(:photo, Utils.download_file(item['img']))
    set(:caption, result)
  end
end

namespace :isthisforthat do
  desc "Get a random IsThisForThat"
  on :default do
    item = Utils.get_json("http://itsthisforthat.com/api.php?json")
    result =<<EOF
#{item['this']} - #{item['that']}
EOF
    set(:text, result)
  end
end

namespace :yesno do
  desc "Yes/No gif, randomally generated"
  on :default do
    item = Utils.get_json("http://yesno.wtf/api/")
    set(:document, Utils.download_file(item['image']))
    set(:text, item['answer'])
  end
end

namespace :amazingfact do
  desc "Get an amazing fact"
  on :default do
    fact = Utils.get_json("http://mentalfloss.com/api/1.0/views/amazing_facts.json?limit=1").first
    set(:text, Sanitize.fragment(fact['nid']))
  end
end

namespace :ynet do
  desc "Ynet latest news"
  on :default do
    response = Faraday.get("http://www.ynet.co.il/Ext/Comp/Ticker/Flash_Ticker_Data/0,12115,L-184,00.html?timestamp=#{Time.now.to_i}")
    doc = Nokogiri::XML(response.body)
    result = []
    doc.css("TickerItems item").each { |item|
      date = item.css("date").children.text
      message = item.css("message").children.text
      result << "#{date} - #{message}"
    }
    set :text,  result.join("\n-\n")
  end
end

namespace :sudo do
  desc "Runs stuff for Super Ninjas"
  on :fact do
    if user.id != 659338
      set :text, "Nope, you are not my boss!\n@zaherm - please note that @#{user.username} is teasing me!"
    else
      res = "No data for #{text}"
      r = Facter::fact(text.to_sym).value rescue nil
      if r
        res = "#{text}:\n#{Utils.tree(r, " ")}"
      end
      set :text, res
    end
  end

  on :exec do
    if user.id != 659338
      set :text, "Nope, you are not my boss!\n@zaherm - please note that @#{user.username} is teasing me!"
    else
      res = %x[#{text}]
      set :text, res
    end
  end
end

namespace :giphy do
  desc "Giphy stuff, random, search, etc"

  Giphy::Configuration.configure do |config|
    config.version = "v1"
    config.api_key = "dc6zaTOxFJmzC" #it's public beta API key, available on their github page
  end

  on :random do
    gif = Giphy.random(text || "")
    set :document, Utils.download_file(gif.image_original_url.to_s)
  end
end

namespace :imgur do
  desc "Fetch random imgur item"
  on :default do
    conn = Faraday.new("https://imgur.com") do |f|
      f.use FaradayMiddleware::FollowRedirects, :limit => 10
      f.adapter Faraday.default_adapter
    end
    doc = Nokogiri::HTML(conn.get("/random").body)
    src = doc.css("#inside div img").attribute('src').value
    if src
      set :caption, doc.css("#image-title").text
      set :photo, Utils.download_file(src)
    else
      set :text, "Something went wrong"
    end
  end
end


namespace :weather do
  desc "Fetch weather status"
  on :default do
    weather = OpenWeather::Current.city(text, :units => 'metric')
    set :text, "#{weather['name']}, #{weather['sys']['country']}: #{weather['main']['temp']}°"
  end
end

namespace :master do
  desc "Respond with My Boss!"
  on :default do
    set :text, "Zaher is my master!"
  end
end

namespace :slave do
  desc "Respond with My Boss!"
  on :default do
    set :text, "Amit Goldberg is my slave!"
  end
end


namespace :inspire do
  desc "Generate an inspiration image"
  on :default do
    resp = Faraday.get("http://inspirobot.me/api", { :generate =>true }) rescue nil
    if resp
      set(:photo, Utils.download_file(resp.body))
    else
      set :text, "Something went wrong"
    end
  end
end

namespace :laugh do
  desc "Send a Beavis and Butthead laugh"
  on :default do
    set :audio, File.join("resources", "laugh.mp3")
  end
end

namespace :shabbat do
  desc "Shabbat Hayom!!"
  on :default do
    set :audio, File.join("resources", "shabbat.mp3")
  end
end


namespace :horoscope do
  desc "Get your Daily horoscope"

  on :default do
    today = Time.now.strftime("%-m/%-d/%Y")
    cache_file = "/tmp/horoscope-#{today.gsub("/","_")}.xml"
    unless File.exists?(cache_file)
      Utils.download_file("http://www.findyourfate.com/rss/horoscope-astrology-feed.asp?mode=view&todate=#{today}", cache_file)
    end
    doc = Nokogiri::XML(File.read(cache_file))
    result = {}
    doc.xpath('//item').each do |i|
      title = i.xpath('title').inner_text
      if title.downcase =~ /#{text}/
        result[:title] = title
        result[:text] = i.xpath('description').inner_text
        break
      end
    end
    set :text, "#{result[:title]}\n----------\n#{result[:text]}"
  end
end

namespace :define do
  desc "Find a word"
  on :default do
    conn = Faraday.new("http://www.thefreedictionary.com") do |f|
      f.use FaradayMiddleware::FollowRedirects, :limit => 10
      f.adapter Faraday.default_adapter
    end
    doc = Nokogiri::HTML(conn.get("/#{text}").body)
    result = doc.css(".ds-single").inner_text rescue nil
    result = "error: could not find a definition for #{text}" if result.empty?
    set :text, result.strip
  end
end

