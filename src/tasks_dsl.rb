module Tasks
  module DSL
    @@routing = {}
    def task(pattern, &block)
      @@routing[pattern] = block
    end
  end
end
