require 'chronic'
require 'redis'
require 'json'
require 'securerandom'

class Scheduler
  def initialize(logger = Logger.new(STDOUT), receiver)
    @logger = logger
    @receiver = receiver
    @redis = Redis.new
  end

  def start!
    while true do
      pattern = Time.now.utc.strftime('job.%Y%m%d%H:%M:*.*')
      keys = @redis.keys(pattern)
      keys.each do |key|
        job = JSON.parse(@redis.get(key)) rescue nil
        next unless job
        @logger.info "Got job = #{job.inspect}"
        @receiver.on_job(job)        
        @redis.del(key)
      end
      sleep 10
    end
  end
  
  def enqueue(task, chat_id, user_id, job)
    ts = Chronic.parse(job['timestamp']).utc
    job_key = ts.strftime('job.%Y%m%d%H:%M:%S.') + SecureRandom.hex.to_s
    @redis.set(job_key, JSON.dump({
      :user_id => user_id,
      :chat_id => chat_id,
      :task => task,
    }.merge(job)))
    @logger.info "Job enqueued = #{job_key}"
  end
end

