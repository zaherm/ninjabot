class Listener
  def initialize(token, logger = Logger.new(STDOUT), receiver)
    @bot = Telegram::Bot::Client.new(token) 
    @logger = logger
    @receiver = receiver
  end

  def listen!
    @bot.listen do |message|
      @logger.info "Listener::listen! message received, content='#{message.inspect}'"
      @receiver.on_message(message) if message
    end
  end
end

