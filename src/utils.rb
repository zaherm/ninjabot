require 'singleton'
require 'faraday'
require 'securerandom'
require 'uri'
class Utils
  def self.download_file(url, filepath = nil)
    response = Faraday.get(url)
    filepath ||= File.join("/tmp", File.basename(URI.parse(url).path))
    File.open(filepath, "wb") { |f|
      f.write response.body
    }
    filepath
  end

  def self.tree(value, padding = "")
    return value if value.is_a?(String) || value.is_a?(Fixnum)

    if value.is_a?(Hash)
      result = []
      value.each { |k, v|
        result << "#{padding}#{k}:#{v.is_a?(Hash) ? "\n" : " "}#{tree(v, padding + " ")}"
      }
      return result.join("\n")
    end

    if value.is_a?(Array)
      return value.join(",")
    end
  end

  def self.get_json(url, params = {})
    json = nil
    begin
      response = Faraday.get(url , params)
      json = JSON.parse(response.body)
    rescue Exception => e
      json = { 'error' => e.message }
    end
    json
  end
end
