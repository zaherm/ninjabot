require 'faraday'
require 'nokogiri'

conn = Faraday.new(:url => 'http://www.quoteland.com') do |faraday|
  faraday.request  :url_encoded             # form-encode POST params
  faraday.response :logger                  # log requests to STDOUT
  faraday.adapter  Faraday.default_adapter  # make requests with Net::HTTP
end


response = conn.post '/random.asp', { 'PAGESIZE' => '1' }
doc = Nokogiri::HTML(response.body)

xpath = "//html/body/table[4]/tr/td[2]/table/tr[4]/td[2]/table/tr/td[1]/p/font/table/tr[3]/td[2]"

category = doc.css("font[size='3'] b")[1].text
quote = doc.css("td font[color='#333333']")[1].text
author = doc.css("td font[size='2'] i a").text
puts category, quote , author
