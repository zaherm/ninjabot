require 'telegram/bot'
require 'json'
require 'logger'
require 'faraday'
require 'chronic'
require 'stock_quote'
require 'nokogiri'
require 'listener'
require 'scheduler'
require 'redis'
require 'ostruct'
require 'commands'
class Bot
  def initialize(token, logger)
    @logger = logger || Logger.new(STDOUT)
    @bot = Telegram::Bot::Client.new(token)
    @listener = Listener.new(token, logger, self)
    @scheduler = Scheduler.new(logger, self)
    @redis = Redis.new
  end

  def start!
    t = []
    t << Thread.new {
      @listener.listen!
    }
    t << Thread.new {
      @scheduler.start!
    }
    t.each(&:join)
  end

  def on_job(job)
    @logger.info "on_job, job='#{job.inspect}'"
    if job['type'] == 'message'
      message = OpenStruct.new
      message.from = OpenStruct.new
      message.from.id = job['user_id']
      message.text = job['text'] || ""
      message.chat = OpenStruct.new
      message.chat.id = job['chat_id']
      message.task_key = job['task'] + ".notify"
      on_message(message)
    end
  end

  def on_message(message)
    return unless message.text
    @logger.info "Bot::on_message was invoked, message='#{message}'"
    result = nil
    begin
      result = Commands::Core.invoke(message.text, { :user => message.from, :chat_id => message.chat.id })
    rescue Exception => e
      @logger.error e.message
      result = nil
    end
    send_result(message.chat.id, result)
  end

  def send_result(chat_id, result)
    return if result.nil?
    @logger.info "Sending result, chat_id='#{chat_id}', result='#{result.inspect}'"

    params = {
      :chat_id => chat_id
    }
    document = result.get(:document)
    if document
      params[:document] = File.new(document)
      @bot.api.sendDocument(params)
    end

    audio = result.get(:audio)
    if audio
      params[:audio] = File.new(audio)
      @bot.api.sendAudio(params)
    end

    text = result.get(:text)
    if !text.empty?
      params.delete(:document)
      params[:text] = result.get(:text)
      @bot.api.sendMessage(params)
    end

    photo = result.get(:photo)
    if photo
      params[:photo] = File.new(photo)
      params[:caption]  = result.get(:caption, "")
      @bot.api.sendPhoto(params)
    end

  end

end
